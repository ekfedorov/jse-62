package ru.ekfedorov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
